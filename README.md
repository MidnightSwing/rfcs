# RFCs

RFCs for the Veloren project

## What exactly is an RFC?

RFCs, meaning *Request For Comment*, are documents detailing ideas for changes within a project that may request non-trivial work to complete, or may affect an aspect of the project in a large way.

In Veloren, RFCs are used as a way to propose changes to the game, engine, associated tools, protocols, and generally any other thing relating to the game including technical, gameplay, artistic, musical or publicising decisions.

RFCs are used in many other projects and organisations and have proven themselves to be an effective way our encouraging good design and engaging a community in technically discussion, as well as providing a history of past changes and the discussion that occured when making them.

## How does the RFC process work?

Generally speaking, RFCs go through several stages throughout their lifetime.

- **Submission**: At this stage, the RFC is written up by whomever first wanted to propose the change. They fork this repository, add the RFC to it, and then open a merge request to have the RFC included in the public repository.

- **Debate**: Members of the community comment on the proposals suggested by the RFC, critiquing it and considering whether how the proposal world fit in to the project. This is an open discussion that occurs in a 'tracking issue' for the RFC on the repository. The original proposer is free to defend their idea, or even make changes to the RFC to suit the criticisms given. Any aggression or hostilities of any kind is strongly frowned upon. Good discussion involes clear, well-explained arguments that give examples and consider the consequences of a change as objectively as possible.

- **Freeze period**: After a certain amount of time, usually at least several days, and when a core developer has concluded that a clear consensus about the RFC has been reached by the community, the RFC is designated as entering a 'freeze period'. This usually lasts at least several days and is a time when changes to the RFC should not take place. This gives all interested members of the community a chance to review the suggestions and consider the effect they may have. If considerable disagreement still persists, the RFC may be 'unfrozen' to allow the original proposer to alter their proposals to suit the debate.

- **Acceptance**: If no serious objections are made during the freeze period, a core developer will mark the proposal as 'accepted'. At this point, a tracking issue for the RFC's implementation will be created in the main Veloren repository containing a checklist of changes that need implementing for the RFC to be considered fully implemented. It is now up to the community to implement these changes, and is considered an official part of the project roadmap. Additionally, the RFC may be assigned a milestone such that it is included in a specific future release.

